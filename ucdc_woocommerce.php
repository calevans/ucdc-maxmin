<?php
/**
 * Plugin Name:     UCDC Woocommerce Integrations
 * Plugin URI:      https://unclecalsdiveclub.com
 * Description:     All the code necessary for UCDC to integrate with woocommerce
 * Author:          Cal Evans
 * Author URI:      https://calevans.com
 * Text Domain:     ucdc-woocomemrce
 * Domain Path:     /languages
 * Version:         1.1.0
 *
 * @package         Ucdc_Woocommerce
 *
 * Blatently ripped off from: https://www.tychesoftwares.com/how-to-set-minimum-and-maximum-allowable-product-quantities-to-be-added-in-woocommerce-cart/
 *
 * */
 $dir = dirname(__FILE__) . '/';
/**
 * Include all the files we need to make this happen
 */
require_once $dir . 'src/UCDC_Woocommerce_Products.php';

/*
 * Kick everything off
 */
(new UCDC_Woocommerce_Products())->init();

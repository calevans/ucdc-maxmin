# UCDC Woocommerce Integrations

(c) 2023 E.I.C.C., Inc.<br />

All the Min/Max code start life here: https://www.tychesoftwares.com/how-to-set-minimum-and-maximum-allowable-product-quantities-to-be-added-in-woocommerce-cart/. If you like it, make sure and thank them. The rest is all mine.


<?php

/**
 * Uncle Cal's Dive Club WooCommerce Integrations for products
 *
 * @todo RENAME to UCDC_WooCommerce
 */
class UCDC_Woocommerce_Products
{

  /**
   * Initialize everything. This is called during WP's INIT action.
   */
  public function init() :void
  {
    add_filter( 'woocommerce_product_data_tabs', [ $this, 'createProductTab' ],10,1 );
    add_action( 'woocommerce_product_data_panels', [ $this, 'createProductPanel' ]);
    add_action( 'woocommerce_product_options_inventory_product_data', [$this,'addMinMaxCheckboxes'] );
    add_action( 'woocommerce_process_product_meta', [$this,'saveFields'] );
    add_filter( 'woocommerce_quantity_input_args', [$this,'fetchFields'], 10, 2 );
    add_filter( 'woocommerce_quantity_input_type', [$this,'showHiddenQtyFilter'] );
    add_filter( 'woocommerce_get_price_html',[$this,'hidePriceFilter'],20,2);
    add_filter( 'woocommerce_related_products', [$this,'hideRelatedProductsFilter'],20,2);
  }

  /**
   * Create the UCDC tab on the product editor
   */
  public function createProductTab( $tabs )
  {
    $tabs['ucdc_product_tab'] = [
      'label' => __( 'UCDC', 'ucdc-woocomemrce' ),
      'priority' => 50,
      'tab_id' => 'ucdc_product_tab', // ID must be unique!
      'default' => true,
      'target'  =>  'ucdc_data_tab',
    ];

    return $tabs;
  }

  /**
   * Create the panel associated with the UCDC tab on the product editor
   */
  public function createProductPanel() {
    echo '<div id="ucdc_data_tab" class="panel woocommerce_options_panel">';

    echo '<div class="options_group">';
    woocommerce_wp_checkbox(
      array(
        'id'          => '_wc_hide_price',
        'label'       => __( 'Hide Price', 'ucdc-woocomemrce' ),
        'desc_tip'    => 'true',
        'description' => __( 'Optional. If checked this product will not display a price.', 'ucdc-woocomemrce' )
      )
    );
    echo '</div>';

    echo '<div class="options_group">';
    woocommerce_wp_checkbox(
      array(
        'id'          => '_wc_hide_related_products',
        'label'       => __( 'Hide Related Products', 'ucdc-woocomemrce' ),
        'desc_tip'    => 'true',
        'description' => __( 'Optional. If checked this product will not display related products.', 'ucdc-woocomemrce' )
      )
    );
    echo '</div>';

    echo '</div>';
  }


  /**
   * @todo Move the hidePriceFilter checkbox off the inventory tab and put it on a
   *       new tab called display.
   */
  public function addMinMaxCheckboxes() {
    echo '<div class="options_group">';
    woocommerce_wp_text_input(
      array(
        'id'          => '_wc_min_qty_product',
        'label'       => __( 'Minimum Quantity', 'ucdc-woocomemrce' ),
        'placeholder' => '',
        'desc_tip'    => 'true',
        'description' => __( 'Optional. Set a minimum quantity limit allowed per order. Enter a number, 1 or greater.', 'ucdc-woocomemrce' )
      )
    );
    echo '</div>';

    echo '<div class="options_group">';
    woocommerce_wp_text_input(
      array(
        'id'          => '_wc_max_qty_product',
        'label'       => __( 'Maximum Quantity', 'ucdc-woocomemrce' ),
        'placeholder' => '',
        'desc_tip'    => 'true',
        'description' => __( 'Optional. Set a maximum quantity limit allowed per order. Enter a number, 1 or greater.', 'woocommerce' )
      )
    );
    echo '</div>';

  }

  /*
   * This function will save the value set to Minimum Quantity and Maximum Quantity options
   * into _wc_min_qty_product and _wc_max_qty_product meta keys respectively
   */
  public function saveFields( $post_id ) {
    $minimum = (int)sanitize_text_field( $_POST['_wc_min_qty_product'] )??0;
    $maximum = (int)sanitize_text_field( $_POST['_wc_max_qty_product'] );
    $hidePrice = sanitize_text_field( $_POST['_wc_hide_price']??false );
    $hideRelatedProducts = sanitize_text_field( $_POST['_wc_hide_related_products']??false );

    if ($minimum > $maximum) {
      $minimum = $maximum;
    }

    $minimum = ( $minimum < 0 )?0:$minimum;
    $minimum = ( $minimum > $maximum )?$maximum:$minimum;

    update_post_meta( $post_id, '_wc_min_qty_product', $minimum );

    if ( $maximum > 0 ) {
      update_post_meta( $post_id, '_wc_max_qty_product', $maximum );
    }

    update_post_meta( $post_id, '_wc_hide_price', $hidePrice );
    update_post_meta( $post_id, '_wc_hide_related_products', $hideRelatedProducts );
  }

  /*
   *
   * Setting minimum and maximum for quantity input args.
   */
  public function fetchFields( $args, $product ) {

    $productId = $product->get_parent_id() ? $product->get_parent_id() : $product->get_id();

    $minimum = $this->getProductMin( $productId );
    $maximum = $this->getProductMax( $productId );
    $hidePrice = $this->getHidePrice( $productId );
    $hideRelatedProducts = $this->getHideRelatedProducts( $productId );

    if ( ! empty( $minimum ) ) {
      // min is empty
      if ( false !== $minimum ) {
        $args['min_value'] = $minimum;
      }
    }

    if ( ! empty( $maximum ) ) {
      // max is empty
      if ( false !== $maximum ) {
        $args['max_value'] = $maximum;
      }
    }

    if ( $product->managing_stock() && ! $product->backorders_allowed() ) {
      $stock = $product->get_stock_quantity();

      $args['max_value'] = min( $stock, $args['max_value'] );
    }

    if ($minimum === $maximum) {
      $args['readonly'] = true;
    }

    if (!empty($args['min_value']) and empty($args['input_value'])) {
      $args['input_value'] = $args['min_value'];
    }
    return $args;
  }

  /*
   * Hide the price if set
   */
  public function hidePriceFilter ( $price, $product ) {
      return $this->getHidePrice( $product->get_id() ) ? '' : $price;
  }

  /**
   * If the chckbox is checked, don't show related products
   */
  public function hideRelatedProductsFilter( $relatedProducts, $productId )
  {
    $hideRelatedProducts = (bool)get_post_meta( $productId, '_wc_hide_related_products', true );

    if ( $hideRelatedProducts === true ) {
      return [];
    }
    return $relatedProducts;
  }

  /**
   * If the field is hidden then show it as readonly. This mainly affects
   * products whose min and max are equal.
   */
  public function showHiddenQtyFilter ( $args ) {
    if ( $args === 'hidden' ) {
      $args = 'text';
    }
    return $args;
  }



  protected function getProductMax( $productId ) {
    $qty = get_post_meta( $productId, '_wc_max_qty_product', true );
    if ( empty( $qty ) ) {
      $limit = false;
    } else {
      $limit = (int) $qty;
    }
    return $limit;
  }

  protected function getProductMin( $productId ) {
    $qty = get_post_meta( $productId, '_wc_min_qty_product', true );
    if ( empty( $qty ) ) {
      $limit = false;
    } else {
      $limit = (int) $qty;
    }
    return $limit;
  }


  protected function getHidePrice( $productId ) {
    return (bool)get_post_meta( $productId, '_wc_hide_price', true );
  }

  protected function getHideRelatedProducts( $productId ) {
    return (bool)get_post_meta( $productId, '_wc_hide_price', true );
  }

}
